const express = require('express');
const app = express();

const PORT = 4200;

app.use(express.static(__dirname + '/app'));
app.get('/', function (req, res,next) {
 res.redirect('/'); 
});
app.listen(PORT, 'localhost');