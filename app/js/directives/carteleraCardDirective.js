app

  .directive('carteleraCard', function () {
    let directive = {};
    directive.restrict = 'E';
    directive.templateUrl = '../js/templates/carteleraCard.html';

    directive.scope = {
      event: '=event',
      redirectTo: '&redirectto'
    };

    return directive;
  });
