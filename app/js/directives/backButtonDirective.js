app

  .directive('backButton', function () {
    let directive = {};

    directive.restrict = 'E';
    directive.template = '<button class="btn btn-outline-dark">{{text}}</button>';
    directive.scope = {
      text: '@text'
    };

    directive.compile = function (element, attributes) {
      const linkFunction = function (scope, element, attributes) {
        element.on('click', function () {
          history.back();
          scope.$apply();
        });
      };
      return linkFunction;
    };

    return directive;
  });
