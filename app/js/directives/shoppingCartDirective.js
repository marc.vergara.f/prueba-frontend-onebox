app

  .directive('shoppingCart', function () {
    let directive = {};
    directive.restrict = 'E';
    directive.templateUrl = '../js/templates/shoppingCart.html';

    directive.scope = {
      reserve: '=reserve',
      shoppingCartRetrieve: '&shoppingcartretrieve'
    };

    return directive;
  });
