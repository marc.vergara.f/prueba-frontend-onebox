app

.controller('mainCtrl', function (localDataService) {

  const vm = this;
  vm.navbarTemplate = './js/templates/navbar.html';
  vm.navTitle;

  const setNavTitle = function(){
    vm.navTitle = localDataService.getNavTitle();
  };

  localDataService.registerObserverCallback(setNavTitle);


})
