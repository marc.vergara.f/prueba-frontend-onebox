app

.factory('eventsService', function ($http) {

  const vm = this;
  const json = '.json';
  const eventsUrl = 'assets/data/events';
  const eventInfoUrl = 'assets/data/event-info-';

  vm.getEvents = () => {
    return $http.get(`${eventsUrl}${json}`);
  };

  vm.getEventInfo = (id) => {
    return $http.get(`${eventInfoUrl}${id}${json}`);
  }
  
  return vm;

})