app

  .factory('localDataService', function () {

    const vm = this;
    const events = [];
    const observerCallbacks = [];
    let navTitle;
  
    const notifyObservers = () => {
      angular.forEach(observerCallbacks, function(callback){
        callback();
      });
    };

    vm.registerObserverCallback = function(callback){
      observerCallbacks.push(callback);
    };
  
    vm.setNavTitle = (title) => {
      navTitle = title;
      notifyObservers();
    }

    vm.getNavTitle = () => navTitle;

    vm.modifyEvent = (data) => {
      const actualEvent = events.find(res => res.event.id === data.event.id);
      if(actualEvent) actualEvent.sessions = data.sessions;
      else events.push(data);
    }

    vm.getEvent = () => {
      return events;
    }

    return vm;

  })