app

  .controller('detalleCtrl', function ($routeParams, eventsService, localDataService) {

    const vm = this;
    localDataService.setNavTitle('Sessions');
    vm.event = {};
    vm.sessions = [];
    vm.noSession = false;
    vm.currentEvent;
    vm.sessionsList = './js/templates/sessionList.html';

    const idSession = $routeParams.idSession;

    eventsService.getEventInfo(idSession)
      .then(resp => {
        let data;
        const exists = checkIfEventExists(resp);
        exists ? data = getCurrentEvents(resp) : data = resp.data;
        setData(data, exists);
        getEvents();
      })
      .catch(err => {
        vm.noSession = true;
      })

    const setData = (data, exists) => {
      vm.event = { ...data.event };
      vm.sessions = [...data.sessions];

      if (!exists) {
        vm.sessions.map(res => {
          res.id = vm.event.id + res.date;
          res.reserves = 0;
        })
      }
    }

    vm.add = (id) => {
      if (getCurrentSession(id).reserves < getCurrentSession(id).availability) getCurrentSession(id).reserves++;
      modifyEvent();
    }

    vm.retrieve = (id) => {
      if (getCurrentSession(id).reserves > 0) getCurrentSession(id).reserves--;
      modifyEvent();
    }

    const getCurrentSession = (id) => {
      return vm.sessions.find(ses => ses.id == id);
    }

    vm.shoppingCartRetrieve = (idCart, idSession) => {
      const events = localDataService.getEvent();
      const eventIndex = events.findIndex(ev => ev.event.id == idCart);
      const session = events[eventIndex].sessions.find(ses => ses.id == idSession)
      session.reserves--;
      getEvents();
    }

    const modifyEvent = () => {
      event = { event: vm.event, sessions: vm.sessions };
      localDataService.modifyEvent(event);
      getEvents();
    }

    const getEvents = () => {
      events = localDataService.getEvent();
      if (events.length > 0) {
        let totalSessions = events.map(data => data.sessions.some(value => value.reserves > 0));
        totalSessions.map((res, index) => {
          if(!res) events.splice(index, 1);
        });
      }
      vm.currentEvent = localDataService.getEvent();
    }

    const getCurrentEvents = (event) => {
      const events = localDataService.getEvent();
      const currentEvent = events.find(ev => ev.event.id == event.data.event.id);
      return currentEvent;
    }

    const checkIfEventExists = (event) => {
      if (getCurrentEvents(event)) return true;
      else return false;
    }

  })
