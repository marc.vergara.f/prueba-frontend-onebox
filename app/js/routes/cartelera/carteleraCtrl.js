app

  .controller('carteleraCtrl', function ($window, eventsService, localDataService) {

    const vm = this;
    localDataService.setNavTitle('Catalog');
    vm.redirectTo = (id) => $window.location.href = `#!/sessions/${id}`;
    vm.events = [];
    vm.totalEvents;
    vm.column = 'endDate';
    vm.reverse = true;
    vm.filter = './js/templates/filter.html';
    vm.errors = false;

    eventsService.getEvents()
      .then(resp => {
        vm.events = [...resp.data];
        vm.totalEvents = `Hay un total de ${resp.data.length} eventos disponibles!`;
      })
      .catch(err => {
        vm.errors = true;
      });


    vm.selectChange = (id) => {
      switch (+id) {
        case 1:
          vm.column = 'endDate';
          vm.reverse = true;
          break;
        case 2:
          vm.column = 'endDate';
          vm.reverse = false;
          break;
        case 3:
          vm.column = 'startDate';
          vm.reverse = true;
          break;
        case 4:
          vm.column = 'startDate';
          vm.reverse = false;
          break;
        default:
          break;
      }
    }

  })
