app.config(function ($routeProvider) {

  $routeProvider
    .when('/', {
      templateUrl: 'js/routes/cartelera/cartelera.html',
      controller: 'carteleraCtrl',
      controllerAs : 'vm'
    })
    .when('/sessions/:idSession', {
      templateUrl: 'js/routes/detalle/detalle.html',
      controller: 'detalleCtrl',
      controllerAs : 'vm'
    })
    .otherwise({
      redirectTo: '/'
    })

})